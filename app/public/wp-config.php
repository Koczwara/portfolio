<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

if(strstr($_SERVER['SERVER_NAME'], 'portfolio.local')) {
	define( 'DB_NAME', 'local' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST', 'localhost' );
} else {
	define( 'DB_NAME', 'dbuzqdy3gk6c6u' );
	define( 'DB_USER', 'ujpznjjymqfhw' );
	define( 'DB_PASSWORD', 'PortfolioPatrycji' );
	define( 'DB_HOST', '127.0.0.1' );
}


/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pUo0LnEK6BF/eP08BG4dDL7ezIbUkMrtJl7X4I9GFMNAwp4UhRcPdiHHAt5KDSsoOhSBOy4OjeEDRSWuMMXymw==');
define('SECURE_AUTH_KEY',  'NVpQhGytSWmmlDSh2jGugbo3C8yIIH46Pwh1Rmol2Pcd8pl8xOZWgtad6NGN28L9JMZYg4Cc08QFCOwpw0Mhjw==');
define('LOGGED_IN_KEY',    'zkdLDHOaprj8M+wFNtHn71zudzY5EMEMic32gk7GCLWyaLEPby4KevIgdpiLHrZyl8jqn4L9j/VTO3xFn56p3Q==');
define('NONCE_KEY',        'PrlZjq0j433jGDvV7pT8WCN71j5QTMDKBUjwhXwvVoVwhiyPYw7pH+PdCxl9VXBuosoFcvNp7H65HSgBA7qZqw==');
define('AUTH_SALT',        'pVdjdDUUiZot7o+DZFQNZ3+eaoHNLxCHSQ92xsab0eWDAx7UA3Wd4iR4++jfXxQYuMLQg2272cvy7oYrWscktA==');
define('SECURE_AUTH_SALT', 'ukSJKx8ynQfx2YEwnKeeJ60Lv0/pY5sl/TLBr9qhZvWXtk0/A9sAjxLwsfQZ5jM7OEmPBG0992gaU+Oa6dJf8Q==');
define('LOGGED_IN_SALT',   'C9iE+5NpIdJU2ag6/NZSdnyAf9OIVcFrsPfWqY1FFCb8fh5YHVQvFl9i7Htyf/ZLE5D+S7JLUy9qrsyjata6mA==');
define('NONCE_SALT',       'aNgJl1fMNynS8/D27CcfyA54H7x4v88QtdqVkWOsnQfM4FMv7fdush6vtZWRHm9Crqq6pbFJS3aruFKbduwDEg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
