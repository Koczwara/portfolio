<?php get_header(); ?>

<?php get_template_part('partials/main'); ?>
<?php get_template_part('partials/services'); ?>
<?php get_template_part('partials/tiles'); ?>
<?php get_template_part('partials/projects'); ?>
<?php get_template_part('partials/contact'); ?>

<?php get_footer();

?>
