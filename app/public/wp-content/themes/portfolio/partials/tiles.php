<?php
  $tiles_one_title = get_field('tiles_one_title');
  $tiles_one_description = get_field('tiles_one_description');
  $tiles_two_title = get_field('tiles_two_title');
  $tiles_two_description = get_field('tiles_two_description');
  $tiles_three_title = get_field('tiles_three_title');
  $tiles_three_description = get_field('tiles_three_description');
  $tiles_four_title = get_field('tiles_four_title');
  $tiles_four_description = get_field('tiles_four_description');
  $tiles_five_title = get_field('tiles_five_title');
  $tiles_five_description = get_field('tiles_five_description');
  $tiles_six_title = get_field('tiles_six_title');
  $tiles_six_description = get_field('tiles_six_description');
?>

<div class="tiles container">
    <div class="dots" style="background-image: url(<?php echo get_theme_file_uri('/images/dots.jpeg')?>);" ></div>
        <div class="card card-1">
            <div class="icon">
                <i class="fa fa-cogs" aria-hidden="true"></i>
            </div>
            <h1 class="card-title">
                <?= $tiles_one_title ?>
            </h1>
            <p class="card-description">
                <?= $tiles_one_description ?>
            </p>
        </div>
        <div class="card card-1">
            <div class="icon">
                <i class="fa fa-edit" aria-hidden="true"></i>
            </div>
            <h1 class="card-title">
                <?= $tiles_two_title ?>
            </h1>
            <p class="card-description">
                <?= $tiles_two_description ?>
            </p>
        </div>
        <div class="card card-1">
            <div class="icon">
                <i class="fa fa-user" aria-hidden="true"></i>
            </div>
            <h1 class="card-title">
                <?= $tiles_three_title ?>
            </h1>
            <p class="card-description">
                <?= $tiles_three_description ?>
            </p>
        </div>
        <div class="card card-1">
            <div class="icon">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </div>
            <h1 class="card-title">
                <?= $tiles_four_title ?>
            </h1>
            <p class="card-description">
                <?= $tiles_four_description ?>
            </p>
        </div>
        <div class="card card-1">
            <div class="icon">
                <i class="fa fa-cog" aria-hidden="true"></i>
            </div>
            <h1 class="card-title">
                <?= $tiles_five_title ?>
            </h1>
            <p class="card-description">
                <?= $tiles_five_description ?>
            </p>
        </div>
        <div class="card card-1">
            <div class="icon">
                <i class="fa fa-file" aria-hidden="true"></i>
            </div>
            <h1 class="card-title">
                <?= $tiles_six_title ?>
            </h1>
            <p class="card-description">
                <?= $tiles_six_description ?>
            </p>
        </div>
</div>