<?php
  $main_img = get_field('main_img');
  $main_description = get_field('main_description');
  $main_button_first = get_field('main_button_first');
  $main_button_second = get_field('main_button_second');
?>


<section class="page-banner" style="background-image: url( <?= $main_img ?> );" id="home" >
      <div class="page-banner__overlay">
        <div id="intro" class="">
          <div class="container">
            <h4 class="page-banner--description" data-aos="zoom-in" data-aos-duration="2000">
              <?= $main_description ?>
            </h4>
              <a href="#services" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" class="button--place">
                  <button class="btn-primary btn-primary--black btn-primary-main">
                    <?= $main_button_first ?>
                  </button>
              </a>
            <a href="#portfolio" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="1000">
                <button class="btn-primary btn-primary--white">
                  <?= $main_button_second ?>
                </button>
            </a>
              <nav>
                <ul class="min-list social-icons-list group">
                  <li>
                    <a href="https://gitlab.com/Koczwara" target="_blank"><i class="fa fa-gitlab" aria-hidden="true"></i></a>
                  </li>
                  <li>
                    <a href="https://www.linkedin.com/in/patrycja-koczwara-987876178/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                  </li>
                </ul>
              </nav>
              </div>
          </div>
        </div>
  </div>
</section>