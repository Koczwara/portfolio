<?php
  $technology_title = get_field('technology_title');
?>
<div class="technology page-section">
    <h3 class="technology--title">
        <?= $technology_title ?>
    </h3>
    <div class="glide hero-slider container" id="glide1">
       <div class="glide__track" data-glide-el="track">
         <ul class="glide__slides">
         <li class="hero-slider__slide">
               <div class="hero-slider__box">
                    <img src="<?php echo get_theme_file_uri('/images/vue.png')?>" alt="vue" loading="lazy">
               </div>
         </li>
         <li class="hero-slider__slide">
                <div class="hero-slider__box">
                    <img src="<?php echo get_theme_file_uri('/images/npm.png')?>" alt="npm" loading="lazy">
                </div>
         </li>
         <li class="hero-slider__slide">
            <div class="hero-slider__box">
                <img src="<?php echo get_theme_file_uri('/images/php.png')?>" alt="php" loading="lazy">
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box">
                <img src="<?php echo get_theme_file_uri('/images/js.png')?>" alt="java script" loading="lazy">
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box">
                <img src="<?php echo get_theme_file_uri('/images/html.png')?>" alt="html" loading="lazy">
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box">
                <img src="<?php echo get_theme_file_uri('/images/wordpress.png')?>" alt="wordpress" loading="lazy">
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box">
                <img src="<?php echo get_theme_file_uri('/images/webpack.png')?>" alt="webpack" loading="lazy">
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box">
                <img src="<?php echo get_theme_file_uri('/images/css.png')?>" alt="css" loading="lazy">
            </div>
        </li>
      </ul>
    </div> 
  </div>
</div>