<?php
  $tools_title = get_field('tools_title');
  $tools_box_one_title = get_field('tools_box_one_title');
  $tools_box_one_description = get_field('tools_box_one_description');
  $tools_box_two_title = get_field('tools_box_two_title');
  $tools_box_two_description = get_field('tools_box_two_description');
  $tools_box_three_title = get_field('tools_box_three_title');
  $tools_box_three_description = get_field('tools_box_three_description');
?>

<div class="tools page-section">
    <h3 class="tools--title">
      <?= $tools_title ?>
    </h3>
    <div class="glide hero-slider container" id="glide2">
    <div class="dots only-desktop" style="background-image: url(<?php echo get_theme_file_uri('/images/dots.jpeg')?>);" ></div>
       <div class="glide__track" data-glide-el="track">
         <ul class="glide__slides">
         <li class="hero-slider__slide">
               <div class="hero-slider__box">
                  <p class="hero-slider__box--min">
                    <?= $tools_box_one_title ?>
                  </p>
                  <p class="hero-slider__description">
                    <?= $tools_box_one_description ?>
                  </p>
               </div>
         </li>
         <li class="hero-slider__slide">
                <div class="hero-slider__box">
                    <p class="hero-slider__box--min">
                      <?= $tools_box_two_title ?>
                    </p>
                    <p class="hero-slider__description">
                      <?= $tools_box_two_description ?>
                    </p>
                </div>
         </li>
         <li class="hero-slider__slide">
            <div class="hero-slider__box">
               <p class="hero-slider__box--min">
                <?= $tools_box_three_title ?>
               </p>
               <p class="hero-slider__description">
                <?= $tools_box_three_description ?>
               </p>
            </div>
        </li>
      </ul>
    </div> 
  </div>
</div>

