<?php
  $about_img = get_field('about_img');
  $about_title = get_field('about_title');
  $about_subtitle = get_field('about_subtitle');
  $about_description = get_field('about_description');
  $about_button = get_field('about_button');
?>
<section class="about-me-banner" style="background-image: url(<?= $about_img ?>);" id="home" >
      <div class="about-me-banner__overlay">
        <div id="intro" class="">
            <div class="container">
                <h4 class="about-me-banner--title" data-aos="zoom-in" data-aos-duration="2000">
                  <?= $about_title ?>
                </h4>
                <h2 class="about-me-banner--description">
                  <?= $about_subtitle ?>
                </h2>
                <p class="about-me-banner--subtitle"> 
                  <?= $about_description ?>
                </p>
                <a href="#about-me" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" class="button--place">
                  <button class="btn-primary btn-primary--black btn-primary-main">
                    <?= $about_button ?>
                  </button>
              </a>
            </div>
          </div>
        </div>
  </div>
</section>