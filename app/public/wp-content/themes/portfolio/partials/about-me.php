<?php
  $about_me_title = get_field('about_me_title');
  $about_me_description = get_field('about_me_description');
?>
<section class="about-me container container--narrow page-section" id="about-me" data-aos="fade-up" data-aos-duration="2000">
      <div class="container about-me__content">
        <div class="about-me__col-one">
        <h3 class="">
          <?= $about_me_title ?>
        </h3>
        <p class="about-me__content--description">
          <?= $about_me_description ?>
        </p>
        </div>
      </div>
  </section>