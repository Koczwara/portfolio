<section class="contact-section">
  <div class="container container--narrow page-section t-center" id="contact">
    <div class="contact__content">
      <h3 class="contact__content--title" data-aos="fade-up" data-aos-duration="2000">
        <?php the_field('contact_title'); ?>
      </h3>   
      <p class="contact__content--description" data-aos="fade-up" data-aos-duration="2000">
        <?php the_field('contact_description'); ?>
      </p>
      <a href="mailto:patrycjakoczwara@gmail.com" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500">   
        <button class="btn-primary btn-primary--blue">
           <?php the_field('contact_button'); ?>
        </button>
      </a>        
    </div>
  </div>
</section>