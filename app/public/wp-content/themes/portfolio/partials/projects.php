<?php
  $project_one_title = get_field('project_one_title');
  $project_one_description = get_field('project_one_description');
  $project_one_desktop_image = get_field('project_one_desktop_image');
  $project_one_mobile_image = get_field('project_one_mobile_image');
  $project_one_link = get_field('project_one_link');
  $project_two_title = get_field('project_two_title');
  $project_two_description = get_field('project_two_description');
  $project_two_desktop_image = get_field('project_two_desktop_image');
  $project_two_mobile_image = get_field('project_two_mobile_image');
  $project_two_link = get_field('project_two_link');
  $project_three_title = get_field('project_three_title');
  $project_three_description = get_field('project_three_description');
  $project_three_desktop_image = get_field('project_three_desktop_image');
  $project_three_mobile_image = get_field('project_three_mobile_image');
  $project_three_link = get_field('project_three_link');
  $project_four_title = get_field('project_four_title');
  $project_four_description = get_field('project_four_description');
  $project_four_desktop_image = get_field('project_four_desktop_image');
  $project_four_mobile_image = get_field('project_four_mobile_image');
  $project_four_link = get_field('project_four_link');
  $project_five_title = get_field('project_five_title');
  $project_five_description = get_field('project_five_description');
  $project_five_desktop_image = get_field('project_five_desktop_image');
  $project_five_mobile_image = get_field('project_five_mobile_image');
  $project_five_link = get_field('project_five_link');
  $project_six_title = get_field('project_six_title');
  $project_six_description = get_field('project_six_description');
  $project_six_desktop_image = get_field('project_six_desktop_image');
  $project_six_mobile_image = get_field('project_six_mobile_image');
  $project_six_link = get_field('project_six_link');
?>

<div class="projects" id="portfolio">
    <h3 data-aos="fade-up" data-aos-duration="2000">
      <?php the_field('portfolio_title'); ?>
    </h3>

    <div id="top"></div> 
    <section class="gallery">
      <div class="portfolio"> 
      <a href="#" class="close"></a>
        <div class="container">
          <div class="row ul">
            <?php if (!empty($project_one_desktop_image)) { ?>
              <div class="col-12 col-lg-4 portfolio-box">
                  <a class="image" href="#item01">
                    <img src="<?= $project_one_desktop_image ?>" alt="<?= $project_one_title ?>" loading="lazy">
                  </a>         
              </div>
            <?php } ?>          
            <?php if (!empty($project_two_desktop_image)) { ?>
              <div class="col-12 col-lg-4 portfolio-box">
                  <a class="image" href="#item02">
                    <img src="<?= $project_two_desktop_image ?>" alt="<?= $project_two_title ?>" loading="lazy">
                  </a>      
              </div>
            <?php } ?>

            <?php if (!empty($project_three_desktop_image)) { ?>
              <div class="col-12 col-lg-4 portfolio-box">
                  <a class="image" href="#item03">
                    <img src="<?= $project_three_desktop_image ?>" alt="<?= $project_three_title ?>" loading="lazy">
                  </a>       
              </div>
            <?php } ?>

            <?php if (!empty($project_four_desktop_image)) { ?>
              <div class="col-12 col-lg-4 portfolio-box">
                  <a class="image" href="#item04">
                    <img src="<?= $project_four_desktop_image ?>" alt="<?= $project_four_title ?>" loading="lazy">
                  </a>   
              </div>
            <?php } ?>

            <?php if (!empty($project_five_desktop_image)) { ?>
              <div class="col-12 col-lg-4 portfolio-box">
                  <a class="image" href="#item05">
                    <img src="<?= $project_five_desktop_image ?>" alt="<?= $project_five_title ?>" loading="lazy">
                  </a>
              </div>
            <?php } ?>

            <?php if (!empty($project_six_desktop_image)) { ?>
            <div class="col-12 col-lg-4 portfolio-box">
                <a class="image" href="#item06">
                  <img src="<?= $project_six_desktop_image ?>" alt="<?= $project_six_title ?>" loading="lazy">
                </a>
            </div>
            <?php } ?>
            
          </div>  
        </div>

      <div id="item01" class="port">     
        <div class="portfolio"> 
        <img src="<?= $project_one_desktop_image ?>" alt="" class="only-mobile">  
          <div class="description">
            <h1><?= $project_one_title ?></h1>
            <p><?= $project_one_description ?></p>
            <a href="<?= $project_one_link ?>" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" target="_blank">   
              <button class="btn-primary btn-primary--blue">
                Zobacz w sieci
              </button>
            </a> 
          </div>
            <img src="<?= $project_one_desktop_image ?>" alt="" class="only-desktop">  
          </div>
        </div> 
      </div> 

      <div id="item02" class="port">      
        <div class="portfolio">
        <img src="<?= $project_two_desktop_image ?>" alt="" class="only-mobile">
          <div class="description">
            <h1><?= $project_two_title ?></h1>
            <p><?= $project_two_description ?></p>
            <a href="<?= $project_two_link ?>" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" target="_blank">   
              <button class="btn-primary btn-primary--blue">
                Zobacz w sieci
              </button>
            </a> 
          </div>
          <img src="<?= $project_two_desktop_image ?>" alt="" class="only-desktop">
        </div> 
      </div>
      <div id="item03" class="port">      
        <div class="portfolio">
        <img src="<?= $project_three_desktop_image ?>" alt="" class="only-mobile">
          <div class="description">
            <h1><?= $project_three_title ?></h1>
            <p><?= $project_three_description ?></p>
            <a href="<?= $project_three_link ?>" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" target="_blank">   
              <button class="btn-primary btn-primary--blue">
                Zobacz w sieci
              </button>
            </a> 
          </div>
          <img src="<?= $project_three_desktop_image ?>" alt="" class="only-desktop">
        </div> 
      </div>
      <div id="item04" class="port">      
        <div class="portfolio">
        <img src="<?= $project_four_desktop_image ?>" alt="" class="only-mobile">
          <div class="description">
            <h1><?= $project_four_title ?></h1>
            <p><?= $project_four_description ?></p>
            <a href="<?= $project_four_link ?>" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" target="_blank">   
              <button class="btn-primary btn-primary--blue">
                Zobacz w sieci
              </button>
            </a> 
          </div>
          <img src="<?= $project_four_desktop_image ?>" alt="" class="only-desktop">
        </div> 
      </div>
      <div id="item05" class="port">      
        <div class="portfolio">
        <img src="<?= $project_five_desktop_image ?>" alt="" class="only-mobile">
          <div class="description">
            <h1><?= $project_five_title ?></h1>
            <p><?= $project_five_description ?></p>
            <a href="<?= $project_five_link ?>" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" target="_blank">   
              <button class="btn-primary btn-primary--blue">
                Zobacz w sieci
              </button>
            </a> 
          </div>
          <img src="<?= $project_five_desktop_image ?>" alt="" class="only-desktop">
        </div> 
      </div>
      <div id="item06" class="port">      
        <div class="portfolio">
        <img src="<?= $project_six_desktop_image ?>" alt="" class="only-mobile">
          <div class="description">
            <h1><?= $project_six_title ?></h1>
            <p><?= $project_six_description ?></p>
            <a href="<?= $project_six_link ?>" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" target="_blank">   
              <button class="btn-primary btn-primary--blue">
                Zobacz w sieci
              </button>
            </a> 
          </div>
          <img src="<?= $project_six_desktop_image ?>" alt="" class="only-desktop">
        </div> 
      </div>

    </section>  
  </div>




