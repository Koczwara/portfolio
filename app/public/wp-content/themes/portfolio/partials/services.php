<?php
  $services_one_title = get_field('services_one_title');
  $services_one_subtitle = get_field('services_one_subtitle');
  $services_one_description = get_field('services_one_description');
  $services_one_img = get_field('services_one_img');
  $services_two_title = get_field('services_two_title');
  $services_two_subtitle = get_field('services_two_subtitle');
  $services_two_description = get_field('services_two_description');
  $services_two_img = get_field('services_two_img');
  $services_three_title = get_field('services_three_title');
  $services_three_subtitle = get_field('services_three_subtitle');
  $services_three_description = get_field('services_three_description');
  $services_three_img = get_field('services_three_img');
  $services_four_title = get_field('services_four_title');
  $services_four_subtitle = get_field('services_four_subtitle');
  $services_four_description = get_field('services_four_description');
  $services_four_img = get_field('services_four_img');
?>

<section class="services-one" id="services">
    <div class="container services-one__content">
      <div class="services-one__col-one">
      <div class="dots" style="background-image: url(<?php echo get_theme_file_uri('/images/dots.jpeg')?>);" ></div>
      <h1 class="services-one__content--title" data-aos="fade-up" data-aos-duration="2000">
        <?= $services_one_title ?>
      </h1>
      <h2 class="services-one__content--subtitle" data-aos="fade-up" data-aos-duration="2000">
        <?= $services_one_subtitle ?>
      </h2>
      <p class="services-one__content--description" data-aos="fade-up" data-aos-duration="2000">
        <?= $services_one_description ?>
      </p>
      </div>
      <div class="services-one__col-two">
        <img src="<?= $services_one_img ?>" alt="strony internetowe" loading="lazy" data-aos="zoom-in" data-aos-duration="2000">     
      </div>
    </div>
</section>

<section class="services">
   <div class="container services__content">
      <div class="services__col-two only-desktop">
        <img src="<?= $services_two_img ?>" alt="sklepy internetowe" loading="lazy" data-aos="zoom-in" data-aos-duration="2000" >
      </div>
      <div class="services__col-one">
      <div class="dots" style="background-image: url(<?php echo get_theme_file_uri('/images/dots.jpeg')?>);" ></div>
         <h1 class="services__content--title" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_two_title ?>
         </h1>
         <h2 class="services__content--subtitle" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_two_subtitle ?>
         </h2>
         <p class="services__content--description" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_two_description ?>
         </p>
      </div>
      <div class="services__col-two only-mobile">
        <img src="<?= $services_two_img ?>" alt="sklepy internetowe" loading="lazy" data-aos="zoom-in" data-aos-duration="2000">
      </div>
   </div>
</section>

<section class="services-one">
      <div class="container services-one__content">
        <div class="services-one__col-one">
        <div class="dots" style="background-image: url(<?php echo get_theme_file_uri('/images/dots.jpeg')?>);" ></div>
        <h1 class="services-one__content--title" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_three_title ?>
        </h1>
        <h2 class="services-one__content--subtitle" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_three_subtitle ?>
        </h2>
        <p class="services-one__content--description" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_three_description ?>
        </p>
        </div>
        <div class="services-one__col-two">
          <img src="<?= $services_three_img ?>" alt="zarządzanie www" loading="lazy" data-aos="zoom-in" data-aos-duration="2000">     
        </div>
      </div>
  </section>

  <section class="services">
   <div class="container services__content">
      <div class="services__col-two only-desktop">
        <img src="<?= $services_four_img ?>" alt="rozwój" loading="lazy" data-aos="zoom-in" data-aos-duration="2000">
      </div>
      <div class="services__col-one">
      <div class="dots" style="background-image: url(<?php echo get_theme_file_uri('/images/dots.jpeg')?>);" ></div>
         <h1 class="services__content--title" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_four_title ?>
         </h1>
         <h2 class="services__content--subtitle" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_four_subtitle ?>
         </h2>
         <p class="services__content--description" data-aos="fade-up" data-aos-duration="2000">
          <?= $services_four_description ?>
         </p>
      </div>
      <div class="services__col-two only-mobile">
        <img src="<?= $services_four_img ?>" alt="rozwój" loading="lazy" data-aos="zoom-in" data-aos-duration="2000">
      </div>
   </div>
</section>