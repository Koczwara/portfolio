<?php
  $offer_img = get_field('offer_img');
  $offer_title = get_field('offer_title');
  $offer_subtitle = get_field('offer_subtitle');
  $offer_button = get_field('offer_button');
?>

<section class="offer-banner" style="background-image: url( <?= $offer_img ?>);" id="home" >
      <div class="offer-banner__overlay">
        <div id="intro" class="">
          <div class="container">
            <h4 class="offer-banner--title" data-aos="zoom-in" data-aos-duration="2000">
              <?= $offer_title ?>
            </h4>
            <h2 class="offer-banner--description">
              <?= $offer_subtitle ?>
            </h2>
              <a href="#price-list" data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500" class="button--place">
                  <button class="btn-primary btn-primary--black btn-primary-main">
                    <?= $offer_button ?>
                  </button>
              </a>
              </div>
          </div>
        </div>
  </div>
</section>