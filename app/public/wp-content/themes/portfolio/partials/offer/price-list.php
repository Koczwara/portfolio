<?php
  $price_list_title = get_field('price_list_title');
  $price_list_subtitle_one = get_field('price_list_subtitle_one');
  $price_list_subtitle_two = get_field('price_list_subtitle_two');
  $box_one_title = get_field('box_one_title');
  $box_two_title = get_field('box_two_title');
  $box_three_title = get_field('box_three_title');
  $box_one_price = get_field('box_one_price');
  $box_two_price = get_field('box_two_price');
  $box_three_price = get_field('box_three_price');
  $box_one_point_one = get_field('box_one_point_one');
  $box_one_point_two = get_field('box_one_point_two');
  $box_two_point_one = get_field('box_two_point_one');
  $box_two_point_two = get_field('box_two_point_two');
  $box_three_point_one = get_field('box_three_point_one');
  $box_three_point_two = get_field('box_three_point_two');
  $box_shops_one_title = get_field('box_shops_one_title');
  $box_shops_two_title = get_field('box_shops_two_title');
  $box_shops_three_title = get_field('box_shops_three_title');
  $box_shops_one_price = get_field('box_shops_one_price');
  $box_shops_two_price = get_field('box_shops_two_price');
  $box_shops_three_price = get_field('box_shops_three_price');
  $box_shops_one_point_one = get_field('box_shops_one_point_one');
  $box_shops_one_point_two = get_field('box_shops_one_point_two');
  $box_shops_two_point_one = get_field('box_shops_two_point_one');
  $box_shops_two_point_two = get_field('box_shops_two_point_two');
  $box_shops_three_point_one = get_field('box_shops_three_point_one');
  $box_shops_three_point_two = get_field('box_shops_three_point_two');
?>
<div class="container price-list" id="price-list">
    <h4 class="price-list--title" data-aos="zoom-in" data-aos-duration="2000">
        <?= $price_list_title ?>
    </h4>
    <h3 class="price-list--subtitle" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
        <?= $price_list_subtitle_one ?>
    </h3>

    <div class="price-list--tiles">
        <div class="card card-1">
            <h1 class="card-title">
                <?= $box_one_title ?>                
            </h1>
            <p class="card-description">
                od
                <span class="card-description--price">
                  <?= $box_one_price ?> 
                </span> netto
            </p>
            <ul>
                <li> 
                    <?= $box_one_point_one ?> 
                </li>
                <li>
                    <?= $box_one_point_two ?>
                </li>
            </ul>
        </div>
        <div class="card card-1">
            <h1 class="card-title">
                <?= $box_two_title ?> 
            </h1>
            <p class="card-description">
                od
                <span class="card-description--price">
                   <?= $box_two_price ?>
                </span> netto
            </p>
            <ul>
                <li>
                    <?= $box_two_point_one ?> 
                </li>
                <li>
                    <?= $box_two_point_two ?> 
                </li>
            </ul>
        </div>
        <div class="card card-1">
            <h1 class="card-title">
                <?= $box_three_title ?> 
            </h1>
            <p class="card-description">
                od
                <span class="card-description--price">
                   <?= $box_three_price ?>
                </span> netto
            </p>
            <ul>
                <li>
                    <?= $box_three_point_one ?> 
                </li>
                <li>
                    <?= $box_three_point_two ?> 
                </li>
            </ul>
        </div>
    </div>

    <h4 class="price-list--title price-list--title--two" data-aos="zoom-in" data-aos-duration="2000">
        <?= $price_list_title ?>
    </h4>
    <h3 class="price-list--subtitle" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
        <?= $price_list_subtitle_two ?>
    </h3>

    <div class="price-list--tiles">
        <div class="card card-1">
            <h1 class="card-title">
                <?= $box_shops_one_title ?> 
            </h1>
            <p class="card-description">
                od
                <span class="card-description--price">
                     <?= $box_shops_one_price ?>
                </span> netto
            </p>
            <ul>
                <li>
                    <?= $box_shops_one_point_one ?>
                </li>
                <li>
                    <?= $box_shops_one_point_two ?>
                </li>
            </ul>
        </div>
        <div class="card card-1">
            <h1 class="card-title">
                <?= $box_shops_two_title ?> 
            </h1>
            <p class="card-description">
                od
                <span class="card-description--price">
                   <?= $box_shops_two_price ?>
                </span> netto
            </p>
            <ul>
                <li>
                    <?= $box_shops_two_point_one ?>
                </li>
                <li>
                    <?= $box_shops_two_point_two ?>
                </li>
            </ul>
        </div>
        <div class="card card-1">
            <h1 class="card-title">
                <?= $box_shops_three_title ?> 
            </h1>
            <p class="card-description">
                od
                <span class="card-description--price">
                  <?= $box_shops_three_price ?>
                </span> netto
            </p>
            <ul>
                <li>
                    <?= $box_shops_three_point_one ?>
                </li>
                <li>
                    <?= $box_shops_three_point_two ?>
                </li>
            </ul>
        </div>
    </div>
</div>