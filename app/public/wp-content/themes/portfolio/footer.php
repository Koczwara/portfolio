<footer class="site-footer" id="footer">
   <div class="site-footer__inner container">
        <div class="group">
         <div class="site-footer__col-one">        
         <h1 class="school-logo-text--footer">
           <a href="<?php echo site_url() ?>" class="logo">PATRYCJA<br>KOCZWARA<span class="logo2">Web-Developer<span></a>
          </h1>
        </div>

          <div class="site-footer__col-two">
            <h6 class="site-footer--title">Kontakt</h6>
            <p><a class="site-footer__link" href="mailto:patrycjakoczwara@gmail.com">patrycjakoczwara@gmail.com</a></p>
            <p><a class="site-footer__link" href="tel:535 057 977"><i class="fa fa-phone" aria-hidden="true"></i> 535 057 977</a></p>
          </div>
  
          <div class="site-footer__col-three">
            <h6 class="site-footer--title">Social Media</h6>
            <ul>
              <li class="social-icons"><a href="https://www.facebook.com/patrycja.koczwara.9" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li class="social-icons"><a href="https://www.instagram.com/ppat_ka/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
          </div>

          <div class="site-footer__col-four">
            <h6 class="site-footer--title">Mapa Strony</h6>
              <nav class="footer-navigation">
                <ul>
                  <li><a href="<?php echo site_url() ?>">Strona główna</a></li>
                  <li><a href="<?php echo site_url('/oferta')?>">Oferta</a></li>
                  <li><a href="<?php echo site_url('/o-mnie')?>">O mnie</a></li>
                  <li><a href="#portfolio">Portfolio</a></li>
                </ul>
              </nav>
          </div>
        </div>
      </div>
      <div class="site-footer__under__inner container">
        <div class="site-footer__under__inner__col-one">        
            <p class="privacy-policy">polityka prywatności</p>
        </div>
        <div class="site-footer__under__inner__col-two social-icons-list">      
        </div>
        <div class="site-footer__under__inner__col-three">        
            <p>© 2020 | by <a href="http://patrycjakoczwara.pl/" target="_blank">Patrycja Koczwara</a></p>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>