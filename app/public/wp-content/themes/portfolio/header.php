<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
<div id="page-anim-preloader"></div>  

  
  <header class="site-header">
    <div class="container">
      <h1 class="school-logo-text float-left"><a href="<?php echo site_url() ?>" class="logo">PATRYCJA<br> KOCZWARA<br> <p class="logo2">Web-Developer</p></a></h1>
      <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      <div class="site-header__menu group">
        <nav class="nav main-navigation">
          <ul>
            <li <?php if(is_front_page()) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url() ?>">STRONA GŁÓWNA</a></li>
            <li <?php if(is_page('oferta')) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/oferta')?>">SPRAWDŹ OFERTĘ</a></li>
            <li <?php if(is_page('o-mnie')) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/o-mnie')?>">O MNIE</a></li>
            <li><a href="#portfolio">PORTFOLIO</a></li>
            <li><a href="#contact">KONTAKT</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>


 