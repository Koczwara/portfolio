<?php

function portfolio_files() {
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap', '//fonts.googleapis.com/css2?family=Merriweather+Sans:wght@300;400;500;600;700;800&display=swap');
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  
  if (strstr($_SERVER['SERVER_NAME'], 'portfolio.local')) {
    wp_enqueue_script('main-portfolio-js', 'http://localhost:3000/bundled.js', NULL, '1.0', true);
  } else {
    wp_enqueue_script('our-vendors-js', get_theme_file_uri('/bundled-assets/vendors~scripts.7cd84b1e25bcbacaff2d.js'), NULL, '1.0', true);
    wp_enqueue_script('main-portfolio-js', get_theme_file_uri('/bundled-assets/scripts.bf9d068597a15480d48d.js'), NULL, '1.0', true);
    wp_enqueue_style('our-main-styles', get_theme_file_uri('/bundled-assets/styles.bf9d068597a15480d48d.css'));
    wp_enqueue_style('our-vendors-styles', get_theme_file_uri('/bundled-assets/styles.bf9d068597a15480d48d.css'));
  }
}

wp_localize_script('main-portfolio-js', 'portfolioData', array(
  'root_url' => get_site_url()
));

add_action('wp_enqueue_scripts', 'portfolio_files');

function portfolio_features() {
  add_theme_support('title-tag');
}

add_action('after_setup_theme', 'portfolio_features');

add_filter('ai1wm_exclude_content_from_export', 'ignoreCertainFiles');

function ignoreCertainFiles($exclude_filters) {
  $exclude_filters[] = 'themes/portfolio/node_modules';
  return $exclude_filters;
}
