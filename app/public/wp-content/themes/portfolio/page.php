<?php get_header(); ?>
<?php get_template_part('partials/about-me/main'); ?>
<?php get_template_part('partials/about-me'); ?>
<?php get_template_part('partials/about-me/technology'); ?>
<?php get_template_part('partials/about-me/tools'); ?>
<?php get_template_part('partials/projects'); ?>
<?php get_template_part('partials/contact'); ?>

<?php get_footer();

?>