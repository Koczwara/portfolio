import "../css/style.css"
import 'bootstrap/dist/css/bootstrap.css';
import $ from "jquery"
import MobileMenu from "./modules/MobileMenu"
import AOS from 'aos';
import 'aos/dist/aos.css';
import Glide from "@glidejs/glide"


var mobileMenu = new MobileMenu()


if (module.hot) {
  module.hot.accept()
}

AOS.init();

window.addEventListener("scroll", function(){
  var header = document.querySelector("header");
  header.classList.toggle("sticky", window.scrollY > 0)
})

// front

$(document).ready(function(){
  
  setTimeout(function(){
    
    $("#name").removeClass("larger");

    $("#name span").each(function(){
      $(this).css("-webkit-transition-delay",$(this).data("delay")+"ms").css("transition-delay",$(this).data("delay")+"ms");
      $(this).addClass("visible");
    });
    
	}, 1000);    
  
});
function scrollNav() {
  $('.nav ul li a').click(function(){
    $(".active").removeClass("active");     
    $(this).addClass("active");
    
    $('html, body').stop().animate({
      scrollTop: $($(this).attr('href')).offset().top - 160
    }, 300);
    return false;
  });
}
scrollNav();

 // portfolio
 $('.gallery .portfolio-box a').click(function() {
  var itemID = $(this).attr('href');
  $('.gallery').addClass('item_open');
  $(itemID).addClass('item_open');
  return false;
});
$('.close').click(function() {
  $('.port, .gallery').removeClass('item_open');
  return false;
});

$(".gallery .portfolio-box a").click(function() {
  $('html, body').animate({
      scrollTop: parseInt($("#top").offset().top)
  }, 400);
});

 // carousel

 new Glide('#glide1', {
  type: 'carousel',
  autoplay: 2000,
  focusAt: 'center',
  perView: 5,
  breakpoints: {
    800: {
      perView: 3
    }
  }
}).mount();

new Glide('#glide2', {
  type: 'carousel',
  autoplay: 10000,
  focusAt: 'center',
  perView: 3,
  breakpoints: {
    500: {
      perView: 1,
      autoplay: 3000
    },
    800: {
      perView: 2,
      autoplay: 3000
    }
  }
}).mount();

